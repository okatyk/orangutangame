package com.orangutan.game;

import com.orangutan.game.App.SkeletonApp;

public class ShyPanda extends Panda{
	
    public ShyPanda(Map m) {
    	super(m);
    }

    public ShyPanda(String id, Tile t, Map m) {
    	super(m);
		this.id = id;
		currentTile = t;
	}

	@Override
    public void collideWith(GameItem g) {

    }

    @Override
    public void reactionForJingle() {
    	SkeletonApp.printCall("ShyPanda.reactionForJingle()");
        this.setBackPandaInHand(null);
        SkeletonApp.printReturn();

    }

}
