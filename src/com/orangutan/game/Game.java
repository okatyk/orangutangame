package com.orangutan.game;

import com.orangutan.game.view.*;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * @author Barnabas Gnam
 * @since 2019-03-24
 */
public class Game implements KeyListener {

    private static Map map;
    private static View view;
    private static Timer t;
    private static List<Animal> npcAnimals = new ArrayList<>();
    //iranyitott orangutan
    private static Orangutan UserOrangutan ;
    private static List<Drawable> listToDraw;
    private static Thread thread;

    public static void endGame() {
        System.out.println("GAME OVER!");
        t.setGameItemList(new ArrayList<GameItem>());
        listToDraw.clear();
        t.setRunning(false);
        view.endGameDraw();
    }

    public static void startGame() {
        /* Csempek beallitasa */
        map = new Map();
        int tileSize = 75;
        view = new View();
        listToDraw = new ArrayList<>();
        Tile t1 = new Tile();
        t1.name = "t1";
        map.addTile(t1);
        TileView tv1 = new TileView(t1, view, Color.YELLOW, 55, 5, tileSize, "t1");
        listToDraw.add(tv1);

        Tile t2 = new Tile();
        t2.name = "t2";
        map.addTile(t2);
        TileView tv2 = new TileView(t2, view, Color.YELLOW, 135, 30, tileSize, "t2");
        listToDraw.add(tv2);

        Tile t3 = new Tile();
        t3.name = "t3";
        map.addTile(t3);
        TileView tv3 = new TileView(t3, view, Color.YELLOW, 215, 30, tileSize, "t3");
        listToDraw.add(tv3);

        Tile t4 = new Tile();
        t4.name = "t4";
        map.addTile(t4);
        TileView tv4 = new TileView(t4, view, Color.YELLOW, 295, 5, tileSize, "t4");
        listToDraw.add(tv4);

        Tile t5 = new Tile();
        t5.name = "t5";
        map.addTile(t5);
        TileView tv5 = new TileView(t5, view, Color.YELLOW, 375, 5, tileSize, "t5");
        listToDraw.add(tv5);

        Tile t6 = new Tile();
        t6.name = "t6";
        map.addTile(t6);
        TileView tv6 = new TileView(t6, view, Color.YELLOW, 455, 5, tileSize, "t6");
        listToDraw.add(tv6);

        Tile t7 = new Tile();
        t7.name = "t7";
        map.addTile(t7);
        TileView tv7 = new TileView(t7, view, Color.YELLOW, 535, 5, tileSize, "t7");
        listToDraw.add(tv7);

        Tile t8 = new Tile();
        t8.name = "t8";
        map.addTile(t8);
        TileView tv8 = new TileView(t8, view, Color.YELLOW, 620, 5, tileSize, "t8");
        listToDraw.add(tv8);

        Tile t9 = new Tile();
        t9.name = "t9";
        map.addTile(t9);
        TileView tv9 = new TileView(t9, view, Color.YELLOW, 695, 55, tileSize, "t9");
        listToDraw.add(tv9);

        Tile t10 = new Tile();
        t10.name = "t10";
        map.addTile(t10);
        TileView tv10 = new TileView(t10, view, Color.BLUE, 75, 110, tileSize, "t10");
        listToDraw.add(tv10);

        Tile t11 = new Tile();
        t11.name = "t11";
        map.addTile(t11);
        TileView tv11 = new TileView(t11, view, Color.YELLOW, 170, 115, tileSize, "t11");
        listToDraw.add(tv11);

        Tile t12 = new Tile();
        t12.name = "t12";
        map.addTile(t12);
        TileView tv12 = new TileView(t12, view, Color.YELLOW, 170, 195, tileSize, "t12");
        listToDraw.add(tv12);

        Tile t13 = new Tile();
        t13.name = "t13";
        map.addTile(t13);
        TileView tv13 = new TileView(t13, view, Color.YELLOW, 130, 265, tileSize, "t13");
        listToDraw.add(tv13);

        Tile t14 = new Tile();
        t14.name = "t14";
        map.addTile(t14);
        TileView tv14 = new TileView(t14, view, Color.YELLOW, 40, 265, tileSize, "t14");
        listToDraw.add(tv14);

        Tile t15 = new Tile();
        t15.name = "t15";
        map.addTile(t15);
        TileView tv15 = new TileView(t15, view, Color.YELLOW, 5, 175, tileSize, "t15");
        listToDraw.add(tv15);

        Tile t16 = new Tile();
        t16.name = "t16";
        map.addTile(t16);
        TileView tv16 = new TileView(t16, view, Color.YELLOW, 290, 155, tileSize, "t16");
        listToDraw.add(tv16);

        Tile t17 = new Tile();
        t17.name = "t17";
        map.addTile(t17);
        TileView tv17 = new TileView(t17, view, Color.YELLOW, 400, 155, tileSize, "t17");
        listToDraw.add(tv17);

        Tile t18 = new Tile();
        t18.name = "t18";
        map.addTile(t18);
        TileView tv18 = new TileView(t18, view, Color.YELLOW, 490, 95, tileSize, "t18");
        listToDraw.add(tv18);

        Tile t19 = new Tile();
        t19.name = "t19";
        map.addTile(t19);
        TileView tv19 = new TileView(t19, view, Color.YELLOW, 615, 95, tileSize, "t19");
        listToDraw.add(tv19);

        Tile t20 = new Tile();
        t20.name = "t20";
        map.addTile(t20);
        TileView tv20 = new TileView(t20, view, Color.YELLOW, 545, 160, tileSize, "t20");
        listToDraw.add(tv20);

        Tile t21 = new Tile();
        t21.name = "t21";
        map.addTile(t21);
        TileView tv21 = new TileView(t21, view, Color.YELLOW, 545, 250, tileSize, "t21");
        listToDraw.add(tv21);

        Tile t22 = new Tile();
        t22.name = "t22";
        map.addTile(t22);
        TileView tv22 = new TileView(t22, view, Color.YELLOW, 565, 330, tileSize, "t22");
        listToDraw.add(tv22);

        Tile t23 = new Tile();
        t23.name = "t23";
        map.addTile(t23);
        TileView tv23 = new TileView(t23, view, Color.YELLOW, 605, 410, tileSize, "t23");
        listToDraw.add(tv23);

        Tile t24 = new Tile();
        t24.name = "t24";
        map.addTile(t24);
        TileView tv24 = new TileView(t24, view, Color.YELLOW, 690, 410, tileSize, "t24");
        listToDraw.add(tv24);

        ExitTile t25 = new ExitTile();
        t25.name = "t25";
        t25.setEntryTile(t10);
        map.addTile(t25);
        TileView tv25 = new TileView(t25, view, Color.GREEN, 650, 230, tileSize, "t25");
        listToDraw.add(tv25);

        Tile t26 = new Tile();
        t26.name = "t26";
        map.addTile(t26);
        TileView tv26 = new TileView(t26, view, Color.YELLOW, 470, 320, tileSize, "t26");
        listToDraw.add(tv26);

        Tile t27 = new Tile();
        t27.name = "t27";
        map.addTile(t27);
        TileView tv27 = new TileView(t27, view, Color.YELLOW, 400, 360, tileSize, "t27");
        listToDraw.add(tv27);

        Tile t28 = new Tile();
        t28.name = "t28";
        map.addTile(t28);
        TileView tv28 = new TileView(t28, view, Color.YELLOW, 210, 270, tileSize, "t28");
        listToDraw.add(tv28);

        Tile t29 = new Tile();
        t29.name = "t29";
        map.addTile(t29);
        TileView tv29 = new TileView(t29, view, Color.YELLOW, 270, 330, tileSize, "t29");
        listToDraw.add(tv29);

        Tile t30 = new Tile();
        t30.name = "t30";
        map.addTile(t30);
        TileView tv30 = new TileView(t30, view, Color.YELLOW, 350, 290, tileSize, "t30");
        listToDraw.add(tv30);

        Tile t31 = new Tile();
        t31.name = "t31";
        map.addTile(t31);
        TileView tv31 = new TileView(t31, view, Color.YELLOW, 400, 440, tileSize, "t31");
        listToDraw.add(tv31);

        Tile t32 = new Tile();
        t32.name = "t32";
        map.addTile(t32);
        TileView tv32 = new TileView(t32, view, Color.YELLOW, 400, 540, tileSize, "t32");
        listToDraw.add(tv32);

        WeakTile t33 = new WeakTile();
        t33.name = "t33";
        t33.setHealth(2);
        map.addTile(t33);
        TileView tv33 = new TileView(t33, view, Color.RED, 500, 410, tileSize, "t33");
        listToDraw.add(tv33);

        Tile t34 = new Tile();
        t34.name = "t34";
        map.addTile(t34);
        TileView tv34 = new TileView(t34, view, Color.YELLOW, 480, 530, tileSize, "t34");
        listToDraw.add(tv34);

        Tile t35 = new Tile();
        t35.name = "t35";
        map.addTile(t35);
        TileView tv35 = new TileView(t35, view, Color.YELLOW, 560, 480, tileSize, "t35");
        listToDraw.add(tv35);

        Tile t36 = new Tile();
        t36.name = "t36";
        map.addTile(t36);
        TileView tv36 = new TileView(t36, view, Color.YELLOW, 570, 560, tileSize, "t36");
        listToDraw.add(tv36);

        Tile t37 = new Tile();
        t37.name = "t37";
        map.addTile(t37);
        TileView tv37 = new TileView(t37, view, Color.YELLOW, 210, 420, tileSize, "t37");
        listToDraw.add(tv37);

        Tile t38 = new Tile();
        t38.name = "t38";
        map.addTile(t38);
        TileView tv38 = new TileView(t38, view, Color.YELLOW, 305, 420, tileSize, "t38");
        listToDraw.add(tv38);

        Tile t39 = new Tile();
        t39.name = "t39";
        map.addTile(t39);
        TileView tv39 = new TileView(t39, view, Color.YELLOW, 210, 520, tileSize, "t39");
        listToDraw.add(tv39);

        Tile t40 = new Tile();
        t40.name = "t40";
        map.addTile(t40);
        TileView tv40 = new TileView(t40, view, Color.YELLOW, 120, 520, tileSize, "t40");
        listToDraw.add(tv40);

        Tile t41 = new Tile();
        t41.name = "t41";
        map.addTile(t41);
        TileView tv41 = new TileView(t41, view, Color.YELLOW, 20, 410, tileSize, "t41");
        listToDraw.add(tv41);

        WeakTile t42 = new WeakTile();
        t42.name = "t42";
        map.addTile(t42);
        TileView tv42 = new TileView(t42, view, Color.RED, 120, 410, tileSize, "t42");
        listToDraw.add(tv42);

        Tile[] tt1 = {t2, t10};
        t1.setNeighbours(tt1);

        Tile[] tt2 = {t1, t10, t11};
        t2.setNeighbours(tt2);

        Tile[] tt3 = {t4, t11, t16};
        t3.setNeighbours(tt3);

        Tile[] tt4 = {t3, t5, t16};
        t4.setNeighbours(tt4);

        Tile[] tt5 = {t4, t6, t16, t17};
        t5.setNeighbours(tt5);

        Tile[] tt6 = {t5, t17, t18};
        t6.setNeighbours(tt6);

        Tile[] tt7 = {t8, t18, t19};
        t7.setNeighbours(tt7);

        Tile[] tt8 = {t7, t9};
        t8.setNeighbours(tt8);

        Tile[] tt9 = {t8, t19, t25};
        t9.setNeighbours(tt9);

        Tile[] tt10 = {t1, t2, t11, t12, t13, t14, t15};
        t10.setNeighbours(tt10);

        Tile[] tt11 = {t2, t3, t10, t12, t16};
        t11.setNeighbours(tt11);

        Tile[] tt12 = {t10, t11, t13, t16, t28};
        t12.setNeighbours(tt12);

        Tile[] tt13 = {t10, t12, t14, t42};
        t13.setNeighbours(tt13);

        Tile[] tt14 = {t10, t13, t15, t41};
        t14.setNeighbours(tt14);

        Tile[] tt15 = {t10, t14};
        t15.setNeighbours(tt15);

        Tile[] tt16 = {t3, t4, t5, t11, t12, t17, t28, t29, t30};
        t16.setNeighbours(tt16);

        Tile[] tt17 = {t5, t6, t16, t18, t20, t21, t26, t27, t30};
        t17.setNeighbours(tt17);

        Tile[] tt18 = {t6, t7, t17, t20};
        t18.setNeighbours(tt18);

        Tile[] tt19 = {t7, t9, t20, t25};
        t19.setNeighbours(tt19);

        Tile[] tt20 = {t17, t18, t19, t21, t25};
        t20.setNeighbours(tt20);

        Tile[] tt21 = {t17, t20, t22, t25, t26};
        t21.setNeighbours(tt21);

        Tile[] tt22 = {t21, t23, t25, t33};
        t22.setNeighbours(tt22);

        Tile[] tt23 = {t22, t24, t25, t35};
        t23.setNeighbours(tt23);

        Tile[] tt24 = {t23, t25};
        t24.setNeighbours(tt24);

        Tile[] tt25 = {t9, t19, t20, t21, t22, t23, t24};
        t25.setNeighbours(tt25);

        Tile[] tt26 = {t17, t21, t27, t33};
        t26.setNeighbours(tt26);

        Tile[] tt27 = {t17, t26, t30, t31, t38};
        t27.setNeighbours(tt27);

        Tile[] tt28 = {t12, t16, t29, t42};
        t28.setNeighbours(tt28);

        Tile[] tt29 = {t16, t28, t30, t37, t38};
        t29.setNeighbours(tt29);

        Tile[] tt30 = {t16, t17, t27, t29, t38};
        t30.setNeighbours(tt30);

        Tile[] tt31 = {t27, t32, t33, t38};
        t31.setNeighbours(tt31);

        Tile[] tt32 = {t31, t34, t38};
        t32.setNeighbours(tt32);

        Tile[] tt33 = {t22, t26, t31, t34, t35};
        t33.setNeighbours(tt33);

        Tile[] tt34 = {t32, t33, t36};
        t34.setNeighbours(tt34);

        Tile[] tt35 = {t23, t33, t36};
        t35.setNeighbours(tt35);

        Tile[] tt36 = {t34, t35};
        t36.setNeighbours(tt36);

        Tile[] tt37 = {t29, t38, t39, t42};
        t37.setNeighbours(tt37);

        Tile[] tt38 = {t27, t29, t30, t31, t32, t37, t39};
        t38.setNeighbours(tt38);

        Tile[] tt39 = {t37, t38, t40};
        t39.setNeighbours(tt39);

        Tile[] tt40 = {t39, t42};
        t40.setNeighbours(tt40);

        Tile[] tt41 = {t14, t42};
        t41.setNeighbours(tt41);

        Tile[] tt42 = {t13, t28, t37, t40, t41};
        t42.setNeighbours(tt42);

        UserOrangutan = new Orangutan(true,map);
        t31.setItem(UserOrangutan);
        OrangutanView orangutanView = new OrangutanView(UserOrangutan, view, true);
        listToDraw.add(orangutanView);


        LazyPanda p1 = new LazyPanda(map);
        t3.setItem(p1);
        LazyPandaView p1v = new LazyPandaView(p1, view);
        listToDraw.add(p1v);

        JumpyPanda p2 = new JumpyPanda(map);
        t5.setItem(p2);
        JumpyPandaView p2v = new JumpyPandaView(p2, view);
        listToDraw.add(p2v);

        LazyPanda p3 = new LazyPanda(map);
        t19.setItem(p3);
        LazyPandaView p3v = new LazyPandaView(p3, view);
        listToDraw.add(p3v);

        JumpyPanda p4 = new JumpyPanda(map);
        t9.setItem(p4);
        JumpyPandaView p4v = new JumpyPandaView(p4, view);
        listToDraw.add(p4v);

        ShyPanda p5 = new ShyPanda(map);
        t34.setItem(p5);
        ShyPandaView p5v = new ShyPandaView(p5, view);
        listToDraw.add(p5v);

        ShyPanda p6 = new ShyPanda(map);
        t29.setItem(p6);
        ShyPandaView p6v = new ShyPandaView(p6, view);
        listToDraw.add(p6v);

        GameMachine g1 = new GameMachine(20);
        t17.setItem(g1);
        GameMachineView g1v = new GameMachineView(g1, view);
        listToDraw.add(g1v);

        ChocoMachine g2 = new ChocoMachine(20);
        t38.setItem(g2);
        ChocoMachineView g2v = new ChocoMachineView(g2, view);
        listToDraw.add(g2v);

        ArmChair g3 = new ArmChair("fotel1", t36);
        t36.setItem(g3);
        ArmChairView g3v = new ArmChairView(g3, view);
        listToDraw.add(g3v);
        //Cabinetnél a teleport tile-t ne a masik tile-ra adjatok meg mer ugy sose fog mukodni
        Cabinet g4 = new Cabinet("szekreny", t24, t23);
        t24.setItem(g4);
        CabinetView g4v = new CabinetView(g4, view);
        listToDraw.add(g4v);

        Cabinet g5 = new Cabinet("szekreny2", t40, t42);
        t40.setItem(g5);
        CabinetView g5v = new CabinetView(g5, view);
        listToDraw.add(g5v);

        Orangutan npcO = new Orangutan(map);
        t23.setItem(npcO);
        OrangutanView npcOview = new OrangutanView(npcO,view,false);
        listToDraw.add(npcOview);


        g4.setSiblingCabinet(g5);
        g5.setSiblingCabinet(g4);

        view.setDrawables(listToDraw);
        view.repaint();


        t = new Timer(view, UserOrangutan);
        map.setTimer(t);
        t.addItem(UserOrangutan);
        t.addItem(p1);
        t.addItem(p2);
        t.addItem(p3);
        t.addItem(p4);
        t.addItem(p5);
        t.addItem(p6);
        t.addItem(g1);
        t.addItem(g2);
        t.addItem(g3);
        t.addItem(g4);
        t.addItem(g5);
        t.addItem(npcO);
      /*  UserOrangutan.setNextDirection(Direction.D);
        UserOrangutan.move();
        view.repaint();
        UserOrangutan.setNextDirection(Direction.D);
        UserOrangutan.move();
        view.repaint();
        UserOrangutan.setNextDirection(Direction.D);
        UserOrangutan.move();
        view.repaint();
        UserOrangutan.setNextDirection(Direction.D);
        UserOrangutan.move();
        view.repaint();*/
        thread = new Thread(t);
        thread.start();

        npcAnimals.add(p1);
        npcAnimals.add(p2);
        npcAnimals.add(p3);
        npcAnimals.add(p4);
        npcAnimals.add(p5);
        npcAnimals.add(p6);
        npcAnimals.add(npcO);
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
        switch (e.getKeyChar()) {
            case 'a':
                UserOrangutan.setNextDirection(Direction.A);
                break;
            case 's':
                UserOrangutan.setNextDirection(Direction.S);
                break;
            case 'd':
                UserOrangutan.setNextDirection(Direction.D);
                break;
            case 'f':
                UserOrangutan.setNextDirection(Direction.F);
                break;
            case 'g':
                UserOrangutan.setNextDirection(Direction.G);
                break;
            case 'h':
                UserOrangutan.setNextDirection(Direction.H);
                break;
            case 'j':
                UserOrangutan.setNextDirection(Direction.J);
                break;
            case 'k':
                UserOrangutan.setNextDirection(Direction.K);
                break;
            default:
                break;
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        System.out.println("megnyomtam");
    }

    public static void setPlayersDirection(Direction d) {
        UserOrangutan.setNextDirection(d);
    }

    /***
     * Veletlenszeru iranyba lepteti a nem jatekos alltal iranyitott allatokat.
     */
    public static void stepRandomAnimal() {
        Random rand = new Random();
        for (Animal animal : npcAnimals) {
            int randomNumber = rand.nextInt(9);
            animal.setNextDirection(getDirectionByNumber(randomNumber));
        }
    }

    private static Direction getDirectionByNumber(int randomnumber) {

        switch (randomnumber) {
            case 0:
                return Direction.A;
            case 1:
                return Direction.S;

            case 2:
                return Direction.D;

            case 3:
                return Direction.F;

            case 4:
                return Direction.G;

            case 5:
                return Direction.H;

            case 6:
                return Direction.J;

            case 7:
                return Direction.K;

            //Ha 8 ast dob akkor nem lep
            case 8:
                return null;
            default:
                return null;


        }
    }

    public static void removeTile(Tile t) {
        map.removeTile(t);
        view.RemoveDrawable(view.getTileView(t));
    }

    public static void removeAnimal(GameItem p) {
        for (Drawable drawable : listToDraw) {
           if(drawable.getGameItem()!=null && drawable.getGameItem().equals(p)){
               listToDraw.remove(drawable);
           }
        }
    }
}