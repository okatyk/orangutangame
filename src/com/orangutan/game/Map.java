package com.orangutan.game;

import java.util.ArrayList;

public class Map {

    private ArrayList<Tile> tileList = new ArrayList<>();
    private Timer timer;
    private int numOfPandas;

    public void addTile(Tile t) {
        tileList.add(t);
    }

    public void removeTile(Tile t) {
        tileList.remove(t);
    }

    public void decreasePanda(Panda p) {
        timer.removeAnimal(p);
        numOfPandas--;
        if (numOfPandas <= 0) {
            Game.endGame();
        }
    }

    public void killOrangutan(Orangutan o) {
        if (o != null) {
            timer.removeAnimal(o);
        }
    }

    public void setTimer(Timer timer) {
        this.timer = timer;
    }
}
