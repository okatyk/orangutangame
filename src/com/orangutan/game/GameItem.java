package com.orangutan.game;

import com.orangutan.game.App.SkeletonApp;

public abstract class GameItem {
    public abstract void  stat();

    protected Tile currentTile;
    public Tile lastTile;//protohoz

    //a prototipus kimenethez
    public String id;

    public void reactionForJingle() {
    }

    public void reactionForWhistle() {
    }

    public void reactionForArmChair(ArmChair a) {
    }

    public void hitByOrangutan(Orangutan o) {
    }

    public void hitByPanda(Panda p) {
    }

    public abstract void tack();

    public abstract void die();

    public Tile getCurrentTile() {
        return currentTile;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setCurrentTile(Tile currentTile) {
        this.currentTile = currentTile;
        //currentTile.setItem(this);


    }
}