package com.orangutan.game;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.io.*;
import java.util.HashMap;

public class Main {
    private static volatile boolean wPressed = false;

    static boolean randomness = false;
    private static HashMap<String, GameItem> allItems = new HashMap<>();
    private static HashMap<String, Tile> allTile = new HashMap<>();

    public static boolean isWPressed() {
        synchronized (Main.class) {
            return wPressed;
        }
    }

//    /**
//     * Megadja, hogy milyen mozgo objektumok vannak a palyan es hol
//     */
//    public static void listAnimalsLocation() {
//
//        if (allItems.isEmpty()) {
//            System.out.println("Nincs allat a palyan");
//        } else {
//            for (GameItem item : allItems.values()) {
//                if (item.id.startsWith("o") || item.id.startsWith("p")) {
//                    if (item.getCurrentTile() == null) {
//                        System.out.println(item.id + " CurrenTile = null!");
//                    } else {
//                        System.out.println(item.id + ": " +
//                                item.getCurrentTile().id);
//                    }
//                }
//
//            }
//
//        }
//    }
//
//    /**
//     * A parameterben megadott allatot lepteti a megadott iranyban.
//     * Ha a veletlenszeruseg ki van kapcsolva, akkor a pandakat is lehet leptetni.
//     * Feltetelezi, hogy a pandak id-ja p-vel kezdodik, az orangutanok o-val.
//     */
//    public static void step(String id, Direction d) {
//        if (id.startsWith("p") || id.startsWith("o")) {
//            Animal animal = (Animal) allItems.get(id);
//            if (animal == null) {
//                System.out.println("Nincs ilyen elem!");
//            } else {
//                if (randomness && id.startsWith("p")) {
//                    System.out.println("Randomness is on!");
//                } else {
//                    animal.setNextDirection(d);
//                    animal.move();
//
//                }
//            }
//        }
//    }
//
//    /**
//     * Megadja, hogy melyik allat melyik csemperol melyikre lepett
//     */
//    public static void getSteps() {
//        for (GameItem a : allItems.values()) {
//            if (a.lastTile == null) {
//
//            } else {
//                System.out.println(a.id + ": " + a.lastTile.id + " -> "
//                        + a.currentTile.id);
//            }
//
//        }
//    }
//
//    /**
//     * A parameterben megadott szerint allitja a
//     * veletlenszeruseget a pandak mozgasanak.
//     *
//     * @param b on/off
//     */
//    public static void setRandomness(String b) {
//        if (b.equals("on")) {
//            randomness = true;
//        } else if (b.equals("off")) {
//            randomness = false;
//        } else {
//            System.out.println("parameter nem ertelmezheto!");
//        }
//    }
//
//    /**
//     * Kiirja a veletlenszeruseg kapcsolo allapotat
//     */
//    public static void getRandomness() {
//        System.out.println("randomness: " + randomness);
//    }
//
//    /**
//     * Kiirja az osszes panda nevet es az ot kezben tarto allat nevet, soronkent
//     * egy pandat.
//     * Feltelelezi, hogy a pandak id-ja p-vel kezdodik.
//     */
//    public static void listPandas() {
//        for (GameItem animal : allItems.values()) {
//            if (animal.id.startsWith("p")) {
//                try {
//                    Panda p = (Panda) animal;
//                    if (p.getForwardAnimalInHand() == null) {
//                        System.out.println(p.id + ": NULL");
//                    } else {
//                        System.out.println(p.id + ": " + p.getForwardAnimalInHand().id);
//                    }
//
//                } catch (Exception e) {
//                    System.out.println("listpandas: cast hiba!");
//                }
//
//            }
//        }
//    }
//
//    /**
//     * A parameterul adott itemnek a lenyegesebb adatait irja ki consolera
//     *
//     * @param item az objektum, aminek az adatait ki szeretnenk iratni
//     */
//    public static void stat(String item) {
//        if (item != null) {
//            if (item.startsWith("cs") || item.startsWith("wcs")) {
//                allTile.get(item).stat();
//            } else {
//                allItems.get(item).stat();
//            }
//        }
//    }
//
//
//    public static void load(String testName) {
//        File file = new File(testName);
//        System.out.println(file.getAbsolutePath());
//        String line;
//        String csvSplitBy = " ";
//        //kinullazni az elozo listakat
//        allTile.clear();
//        allItems.clear();
//// Elkezdj�k sorrol sorra olvasni
//        //a sorokban space-el vannak elv�lasztva a dolgok
//        //pl. egy orangut�nn�l : Orangutan o1 cs2
//        //ami o1 id-val rendelkez� or�ngut�n a cs2 csemoen all es ezt hozzuk letre
//        try (BufferedReader br = new BufferedReader(new InputStreamReader(
//                new FileInputStream(file), "UTF8"))) {
//            while ((line = br.readLine()) != null) {
//                String[] entity = line.split(csvSplitBy);
//                switch (entity[0]) {
//                    case "Orangutan":
//                        if (entity[1] == null || entity[2] == null) {
//                            System.out.println("Hibas parameterezes");
//                        } else {
//                            Tile tile = new Tile(entity[2]);
//                            allItems.put(entity[1], new Orangutan(entity[1], tile));
//                            tile.setItem(allItems.get(entity[1]));
//                            allTile.put(entity[2], tile);
//
//                        }
//                        break;
//                    case "ShyPanda":
//                        if (entity[1] == null || entity[2] == null) {
//                            System.out.println("Hibas parameterezes");
//                        } else {
//                            Tile tile = new Tile(entity[2]);
//                            allItems.put(entity[1], new ShyPanda(entity[1], tile));
//                            tile.setItem(allItems.get(entity[1]));
//                            allTile.put(entity[2], tile);
//                        }
//                        break;
//                    case "JumpyPanda":
//                        if (entity[1] == null || entity[2] == null) {
//                            System.out.println("Hibas parameterezes");
//                        } else {
//                            Tile tile = new Tile(entity[2]);
//                            allItems.put(entity[1], new JumpyPanda(entity[1], tile));
//                            tile.setItem(allItems.get(entity[1]));
//                            allTile.put(entity[2], tile);
//                        }
//                        break;
//
//                    case "LazyPanda":
//                        if (entity[1] == null || entity[2] == null) {
//                            System.out.println("Hibas parameterezes");
//                        } else {
//                            Tile tile = new Tile(entity[2]);
//                            allItems.put(entity[1], new LazyPanda(entity[1], tile));
//                            tile.setItem(allItems.get(entity[1]));
//                            allTile.put(entity[2], tile);
//                        }
//                        break;
//                    case "EmptyTile":
//                        if (entity[1] != null) {
//                            allTile.put(entity[1], new Tile(entity[1]));
//                        }
//                        break;
//
//                    //OrangutanHoldPanda formátuma: OrangutanHoldPanda o1 p1
//                    //ahol o1 aki tartja
//                    //p1 akit tart
//                    case "OrangutanHoldPanda":
//                        Orangutan o1 = (Orangutan) allItems.get(entity[1]);
//                        Panda p1 = (Panda) allItems.get(entity[2]);
//                        o1.setPanda(p1);
//
//                        break;
//
//                    //PandaHoldPanda: PandaHoldPanda p1 p2
//                    //p1 aki elol lesz, p2 aki hatul
//
//
//                    case "ArmChair":
//                        if (entity[1] == null || entity[2] == null) {
//                            System.out.println("Hibas parameterezes");
//                        } else {
//                            Tile tile = new Tile(entity[2]);
//                            allItems.put(entity[1], new ArmChair(entity[1], tile));
//                            tile.setItem(allItems.get(entity[1]));
//                            allTile.put(entity[2], tile);
//                        }
//                        break;
//                    case "Cabinet":
//                        if (entity[1] == null || entity[2] == null) {
//                            System.out.println("Hibas parameterezes");
//                        } else {
//                            Tile tile = new Tile(entity[2]);
//                            Tile tile2 = new Tile("cs3");
//                            allItems.put(entity[1], new Cabinet(entity[1], tile, tile2));
//                            tile.setItem(allItems.get(entity[1]));
//                            allTile.put(entity[2], tile);
//                        }
//                        break;
//                    // formatum : Siblingcabinet c1 c2
//                    // c1-nek c2 lesz a parja
//                    case "Siblingcabinet":
//                        if (entity[1] == null || entity[2] == null) {
//                            System.out.println("Hibas parameterezes");
//                        } else {
//                            try {
//                                Cabinet c1 = (Cabinet) allItems.get(entity[1]);
//                                Cabinet c2 = (Cabinet) allItems.get(entity[2]);
//                                c1.setSiblingCabinet(c2);
//                            } catch (Exception e) {
//                                // TODO: handle exception
//                            }
//
//                        }
//                        break;
//                    case "ChocoMachine":
//                        if (entity[1] == null || entity[2] == null) {
//                            System.out.println("Hibas parameterezes");
//                        } else {
//                            Tile tile = new Tile(entity[2]);
//                            allItems.put(entity[1], new ChocoMachine(entity[1], tile));
//                            tile.setItem(allItems.get(entity[1]));
//                            allTile.put(entity[2], tile);
//                        }
//                        break;
//                    case "GameMachine":
//                        if (entity[1] == null || entity[2] == null) {
//                            System.out.println("Hibas parameterezes");
//                        } else {
//                            Tile tile = new Tile(entity[2]);
//                            allItems.put(entity[1], new GameMachine(entity[1], tile));
//                            tile.setItem(allItems.get(entity[1]));
//                            allTile.put(entity[2], tile);
//                        }
//                        break;
//                    case "ExitTile":
//                        if (entity[1] != null) {
//                            Tile tile = new Tile(entity[2]);
//                            allTile.put(entity[2], tile);
//                            allTile.put(entity[1], new ExitTile(entity[1], tile));
//
//                        }
//                        //ExitTile l�trehoz�skor a m�sodik param�ter alapj�n l�trej�n m�g egy tile, az entrytile
//                        break;
//                    case "WeakTile":
//                        if (entity[1] != null) {
//                            allTile.put(entity[1], new WeakTile(entity[1]));
//                        }
//                        break;
//                    //Formatum: Szomszedok cs0 cs1 cs2
//                    case "Szomszedok":
//                        Tile[] tiles = new Tile[8];
//                        for (int i = 2; i < entity.length; i++) {
//                            tiles[i - 2] = allTile.get(entity[i]);
//                        }
//                        allTile.get(entity[1]).setNeighbours(tiles);
//
//                        break;
//
//                    //PandaHoldPanda: PandaHoldPanda p1 p2
//                    //p1 aki elol lesz, p2 aki hatul
//
//                    case "PandaHoldPanda":
//                        Panda p = (Panda) allItems.get(entity[1]);
//                        Panda p2 = (Panda) allItems.get(entity[2]);
//                        p.setBackPandaInHand(p2);
//                        p2.setForwardAnimalInHand(p);
//                        break;
//                    //OrangutanStepsToSteal o1
//                    //kettore allitja a stepstostealt
//                    case "OrangutanStepsToSteal":
//                        Orangutan o = (Orangutan) allItems.get(entity[1]);
//                        o.setStepsToSteal(2);
//                        break;
//                    case "WeakTileHealth":
//                        WeakTile w1 = (WeakTile) allTile.get(entity[1]);
//                        w1.setHealth(Integer.parseInt(entity[2]));
//                        break;
//                    //OrangutanSetTile o1 cs1
//                    case "OrangutanSetTile":
//                        Orangutan o3 = (Orangutan) allItems.get(entity[1]);
//                        o3.setCurrentTile(allTile.get(entity[2]));
//                        allTile.get(entity[2]).setItem(o3);
//                        break;
//                    case "PandaSetTile":
//                        Panda p3 = (Panda) allItems.get(entity[1]);
//                        p3.setCurrentTile(allTile.get(entity[2]));
//                        allTile.get(entity[2]).setItem(p3);
//                        break;
//                    case "LazyPandaTired":
//                        LazyPanda p4 = (LazyPanda) allItems.get(entity[1]);
//                        if (entity[2] == "true")
//                            p4.setTired(true);
//                        else
//                            p4.setTired(false);
//                        break;
//
//
//                    //Igy tovabb a gameitemeket ( osszeset)
//
//                    // majd olyan case-eket �rni ahol pl. inhand es akkor id szerint modos�tani a megfelelo gameitemeket amik egymas kezet fogjak stb..
//                    // A GAMEITEMEKNEL legyen egy parameter nelkuli konstruktor + a parameters konstruktor is ( ahogy orangutannak es a pandaknal is van )
//                    default:
//                        break;
//                }
//
//            }
//
//
//        } catch (UnsupportedEncodingException e) {
//            e.printStackTrace();
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }
//
//    /**
//     * Elsuti a megadott gep hangkiadasat explicit.
//     *
//     * @param id a gep, amit megszolaltat, feltetelezi, hogy 'cm' vagy 'gm'-el kezdodik.
//     */
//    public static void signal(String id) {
//        if (id.startsWith("cm") || id.startsWith("gm")) {
//            Machine m = (Machine) allItems.get(id);
//            m.signal();
//        } else {
//            System.out.println("parameter nem ertelmezheto!");
//        }
//
//    }
//
//    /**
//     * Elengedi a megadott orangutan kezeben levo pandakat
//     *
//     * @param id 'o'-val kezdodo azonosito
//     */
//    public static void releasePanda(String id) {
//        if (id.startsWith("o")) {
//            Orangutan orang = (Orangutan) allItems.get(id);
//            orang.releasePandas();
//        } else {
//            System.out.println("parameter nem ertelmezheto!");
//        }
//    }
//
//    public static Direction stringToDir(String dir) {
//        switch (dir) {
//            case "A":
//                return Direction.A;
//            case "S":
//                return Direction.S;
//            case "D":
//                return Direction.D;
//            case "F":
//                return Direction.F;
//            case "G":
//                return Direction.G;
//            case "H":
//                return Direction.H;
//            case "J":
//                return Direction.J;
//            case "K":
//                return Direction.K;
//
//            default:
//                return null;
//        }
//    }

    public static void main(String[] args) {
        Game.startGame();
        KeyboardFocusManager.getCurrentKeyboardFocusManager().addKeyEventDispatcher(new KeyEventDispatcher() {

            @Override
            public boolean dispatchKeyEvent(KeyEvent e) {
                synchronized (Main.class) {
                    switch (e.getID()) {
                        case KeyEvent.KEY_PRESSED:
                            switch (e.getKeyCode()) {
                                case KeyEvent.VK_A:
                                    Game.setPlayersDirection(Direction.A);
                                    break;
                                case KeyEvent.VK_S:
                                    Game.setPlayersDirection(Direction.S);
                                    break;
                                case KeyEvent.VK_D:
                                    Game.setPlayersDirection(Direction.D);
                                    break;
                                case KeyEvent.VK_F:
                                    Game.setPlayersDirection(Direction.F);
                                    break;
                                case KeyEvent.VK_G:
                                    Game.setPlayersDirection(Direction.G);
                                    break;
                                case KeyEvent.VK_H:
                                    Game.setPlayersDirection(Direction.H);
                                    break;
                                case KeyEvent.VK_J:
                                    Game.setPlayersDirection(Direction.J);
                                    break;
                                case KeyEvent.VK_K:
                                    Game.setPlayersDirection(Direction.K);
                                    break;
                                default:
                                    break;
                            }
                        default:
                            break;
                    }
                    return false;
                }
            }
        });

    }
}


