package com.orangutan.game;

import java.util.ArrayList;


import com.orangutan.game.App.SkeletonApp;
import com.orangutan.game.view.View;

public class Timer implements Runnable{

	View view;
	Orangutan userOrangutan;
	private ArrayList<GameItem> gameItemList = new ArrayList<>();
	private volatile boolean running = true;
	public Timer(View v, Orangutan o) {
		view = v;
		userOrangutan = o;
	}

	public void addItem(GameItem g) {
		gameItemList.add(g);
	}
	
	public void removeAnimal(GameItem g) {
		gameItemList.remove(g);
		Game.removeAnimal(g);
	}
	
	public void Tack() {
		for (int i = 0; i < gameItemList.size(); i++) {
			gameItemList.get(i).tack();
		}
	}

	@Override
	public void run() {
		while (running) {
			Tack();
			Game.stepRandomAnimal();
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			view.setPoints(userOrangutan.getPoints());
			view.repaint();
		}
	}

	public ArrayList<GameItem> getGameItemList() {
		return gameItemList;
	}

	public void setGameItemList(ArrayList<GameItem> gameItemList) {
		this.gameItemList = gameItemList;
	}

	public void setRunning(boolean running) {
		this.running = running;
	}
}
