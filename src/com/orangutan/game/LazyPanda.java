package com.orangutan.game;

import com.orangutan.game.App.SkeletonApp;

import com.orangutan.game.App.SkeletonApp;

import java.io.IOException;

public class LazyPanda extends Panda {

    private boolean tired = true;
    private int nappingTime = 0;

    public LazyPanda(Map m) {
		super(m);
	}
    
    public LazyPanda(String id,Tile tile, Map m) {
    	super(m);
        this.id = id;
        super.setCurrentTile(tile);
    }

    @Override

    public void collideWith(GameItem g) {

    }

    @Override
    public void reactionForArmChair(ArmChair a) {
        if (tired) {
            nappingTime = 10;
            this.currentTile = a.getTile();
        }
    }

    public void tack() {
        //System.out.println("napping time?");
        //ry {
        //  nappingTime = System.in.read();
        //System.out.println(nappingTime);
        // catch (IOException e) {
        //  e.printStackTrace();
        //}

        if (nappingTime != 0) {
            nappingTime--;
        } else {
        	
            move();
        }
    }

    @Override
    public void stat() {
        super.stat();
        System.out.println("\t " + "Tired: " + this.isTired());
    }

    public boolean isTired() {
        return tired;
    }

    public void setTired(boolean tired) {
        this.tired = tired;
    }
}
