package com.orangutan.game;

import com.orangutan.game.App.SkeletonApp;

public class JumpyPanda extends Panda {

    public JumpyPanda(String id,Tile tile, Map m) {
        super(m);
    	this.id = id;
        super.setCurrentTile(tile);
    }

    public JumpyPanda(Map m) {
    	super(m);
    }

    @Override
    public void reactionForWhistle() {
        this.currentTile.steppedOn(this);
    }

    @Override
    public void collideWith(GameItem g) {

    }
}
