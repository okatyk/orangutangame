package com.orangutan.game;

import com.orangutan.game.GameItem;
import com.orangutan.game.App.SkeletonApp;

/**
 * @author Bence
 * A szomszedos mezore lepo faradekony pandak ertesitese es tarolasa.
 */
public class ArmChair extends GameItem {

    public Tile getTile(){
        return this.currentTile;
    }

	public ArmChair(String id,Tile tile) {
		this.id = id;
		super.setCurrentTile(tile);
	}

	@Override
	public void stat() {
		System.out.println("Armchair");
		System.out.println("\n");
		System.out.println("\t "+"Tile: "+this.getCurrentTile().getId());
	}

	/* (non-Javadoc)
	 * @see com.orangutan.game.GameItem#tack()
	 * Lekrdezi a szomszedos csempeket, majd a nem null
	 * elemeket reagaltatja a fotelre. Csak a LazaPanda fog
	 * reagalni, aki beleul a fotelbe, ha faradt.
	 */
	@Override
	public void tack() {
		try {
			Tile[] negihbs = currentTile.getNeighbours();
			for (int i = 0; i < negihbs.length; i++) {
				if (negihbs[i] != null) {
					if(negihbs[i].getItem()!=null){
					GameItem it = negihbs[i].getItem();
					it.reactionForArmChair(this);}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();;
		}
		
	}

	@Override
	public void die() {

	}


}

