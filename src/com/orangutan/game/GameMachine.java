package com.orangutan.game;

import com.orangutan.game.App.SkeletonApp;


/**
 * @author Bence
 * Bizonyos idokozonkent csilingel egyet es megkerdezi a csempet,
 * amin all, hogy kik a szomszedos csempek, majd a szomszedos
 * elemeken levo pandakat ertesiti a csilingelesrol.
 */
public class GameMachine extends Machine {

    /*Egyparameteres konstruktor
     * @param timePeriod csilingeles idointervalluma
     */
    public GameMachine(int timePeriod) {
        super(timePeriod);
    }

    public GameMachine(String string, Tile tile) {
        super();
        this.id = string;
        currentTile = tile;

    }

    @Override
    public void stat() {
        System.out.println("GameMachine");
        System.out.println("\n");
        System.out.println("\t " + this.getCurrentTile().getId());

    }

    /* (non-Javadoc)
     * @see com.orangutan.game.GameItem#tack()
     * megkerdezi a csempet, amin all, hogy kik a szomszedos csempei.
     *  A szomszedos csempeket vegigkerdezi, hogy mik vannak rajtuk
     *  es ha nem ures, akkor ertesiti a csempen allo objektumot.
     *  Ha ures a szomszedos csempe, akkor nem hat ra.
     */
    @Override
    public void tack() {
        timeForSound--;
        if (timeForSound <= 0) {
            Tile[] negihbs = currentTile.getNeighbours();

            for (int i = 0; i < negihbs.length; i++) {
                if (negihbs[i] != null) {
                    if (negihbs[i].getItem() != null) {
                        GameItem it = negihbs[i].getItem();

                        it.reactionForJingle();
                    }
                }
            }
            timeForSound = time;
        }

    }

    @Override
    public void die() {

    }


}
