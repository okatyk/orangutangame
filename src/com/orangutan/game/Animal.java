package com.orangutan.game;

import com.orangutan.game.App.SkeletonApp;

/***
 * Animal közös ős
 * Másnéven a mozgó gameitemek őse.
 */
public abstract class Animal extends GameItem{

    private Direction nextDirection = null;
    private Tile nextTile;
    public abstract void collideWith(GameItem g);

    public abstract void move();

    public void tack(){
        this.move();
    }
    public void die(){
    }
    
    public void Enter(){
        this.currentTile.remove();
    }

    public Direction getNextDirection() {
        return nextDirection;
    }

    public void setNextDirection(Direction nextDirection) {
        this.nextDirection = nextDirection;
    }
    
    public void exit(){}

    public Tile getNextTile() {
        return nextTile;
    }

    public void setNextTile(Tile nextTile) {
        this.nextTile = nextTile;
    }
}
