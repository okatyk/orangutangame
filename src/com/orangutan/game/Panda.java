package com.orangutan.game;

import com.orangutan.game.App.SkeletonApp;

/**
 * @author Barnabas Gnam
 * @since 2019-03-21
 */
abstract public class Panda extends Animal {

    private Animal forwardAnimalInHand = null;
    private Panda backPandaInHand = null;
    private Map map;

    public Panda(Map m) {
        map = m;
    }

    /**
     * @param p A panda, akit be�ll�tunk az adott Panda m�g�.
     *          Ha null, akkor a m�g�tte l�v� Pand�kon v�gigfut ez a f�ggv�ny a l�ncon
     *          �s lenull�za a m�g�tte l�v� pand�k backPandaInHand v�ltoz�j�t
     */
    public void setBackPandaInHand(Panda p) {

        if (p != null) {
            if (backPandaInHand == null) {
                backPandaInHand = p;
                p.setForwardAnimalInHand(this);
            } else
                backPandaInHand.setBackPandaInHand(p);
        } else {
            if (backPandaInHand != null) {
                backPandaInHand.setBackPandaInHand(null);
                backPandaInHand.setForwardAnimalInHand(null);
                this.backPandaInHand = null;
            }
        }
    }

    public void setForwardAnimalInHand(Animal a) {
        forwardAnimalInHand = a;
    }

    /**
     * Panda lepese
     */
    public void move() {
        if (forwardAnimalInHand != null) {    //ha nem fogja senki
            if (getNextTile() != null) {    // ha van kovetkezo csempe
                if (backPandaInHand != null) {    //ha van mogotte panda
                    backPandaInHand.setNextTile(currentTile);
                    currentTile = getNextTile();
                    backPandaInHand.move();
                } else {        //ha nincs mogotte panda
                    currentTile = getNextTile();
                }
                setNextTile(null);
            }
        } else {        // ha fogja valaki
            if (getNextDirection() != null) {
                lastTile = currentTile; //proto

                Tile t = this.currentTile.getNeighbour(getNextDirection());
                if (t != null) {

                    if (t.getItem() == null) {
                        t.steppedOn(this);
                    } else {
                        t.getItem().hitByPanda(this);
                    }
              /*  if (getBackPandaInHand() != null) {
                    getBackPandaInHand().setNextDirection(getNextDirection());
                    getBackPandaInHand().move();
                }*/
                } else {
                    System.out.println("nextdirection = null! vagy nincs ott tile");
                }
                //lepes utan kinullazzuk ,hogy kovetkezo tack()nal ne lepjen ha nincs uj irany.
                setNextDirection(null);
            }
        }
    }

    /**
     * Panda utkozik egy orangutannal
     *
     * @param o akivel utkozik
     */
    public void hitByOrangutan(Orangutan o) {
        //Mert k�l�nben ha visszal�p egyet rossz lesz.
        if (forwardAnimalInHand != o) {
            if(o.getInHandPanda()==null){
                o.setPanda(this);
                    forwardAnimalInHand = o;

                /*Helycsere*/
       /*         Tile next = o.currentTile;
                o.currentTile = currentTile;
                o.lastTile = next;
                currentTile.setItem(o);

                this.lastTile = currentTile;
                this.currentTile = next;
                next.setItem(this);*/
            }else {
                o.getInHandPanda().setForwardAnimalInHand(this);
            o.setInHandPanda(this);
            forwardAnimalInHand = o;

            }



        }
    }

    /**
     * Visszaadja a mogott levo pandat, ha van
     *
     * @return panda
     */
    public Panda getBackPandaInHand() {
        return backPandaInHand;
    }

    /**
     * Visszaajda az elotte levo allatot, ha van
     *
     * @return panda vagy orangutan
     */
    public Animal getForwardAnimalInHand() {
        return forwardAnimalInHand;
    }

    @Override
    public void die() {
        map.decreasePanda(this);
    }

    @Override
    public void stat() {
        System.out.println(id);
        System.out.println("\n");
        if (this.getForwardAnimalInHand() != null) {
            System.out.println("\t " + "ForwardedAnimal in Hand: " + this.getForwardAnimalInHand().id);
        } else {
            System.out.println("\t " + "ForwardedAnimal in Hand: NULL");
        }
        if (this.getBackPandaInHand() != null) {
            System.out.println("\t " + "BackPanda in Hand: " + this.getBackPandaInHand().id);
        } else {
            System.out.println("\t " + "BackPanda in Hand: NULL");
        }

    }

    public void releaseAll() {
        forwardAnimalInHand = null;
        if (backPandaInHand != null) {
            backPandaInHand.releaseAll();
            backPandaInHand = null;
        }
    }

}
