package com.orangutan.game.App;

/**
 * @author Barnabas Gnam
 * @since 2019-03-21
 */
public class SkeletonApp {

	public static int tabs = 1;
	/** printCall f?ggv?ny kii?rja a c-t a f?ggv?ny elej?re majd n?veli
	a tabul?tort annyival ah?nyadik f?ggv?ny h?v?s ?s ki?rja a f?ggv?ny
	nev?t.
	@param  s a f?ggv?ny neve amit ki?r
	 *
	 *
	 * */
	public static void printCall(String s) {

		System.out.print('c');
		for (int i = 0; i < tabs; i++) {
			System.out.print("\t");
		}
		System.out.println(s);
		SkeletonApp.tabs++;

	}

	/** printReturn kirakja az r t?pust ?s annyi tabul?torral beljebb
	 * rakja a return sz?t amennyival a hozz? tartoz? f?ggv?ny van majd
	 * cs?kkenti a tabs-ot 1el.
	  *
	  *
	  * */
	public static void printReturn() {
		System.out.print('r');
		SkeletonApp.tabs--;
		for (int i = 0; i < tabs; i++) {
			System.out.print("\t");
		}
		System.out.println("return");


	}
	/** printReturn kirakja az r t?pust ?s annyi tabul?torral beljebb
	 * rakja a return sz?t, amennyivel a hozz? tartoz? f?ggv?ny van, majd
	 * az object ami a visszat?r?si ?rt?ket hozz?f?zi ?s cs?kkenti a tabs-ot 1el.
	 *
	 * @param object a f?ggv?ny visszat?r?si ?rt?ke.
	 * */
	public static void printReturn(String object) {
		System.out.print('r');
		SkeletonApp.tabs--;

		for (int i = 0; i < tabs; i++) {
			System.out.print("\t");
		}
		System.out.println("return "+object);


	}
}
