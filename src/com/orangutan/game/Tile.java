package com.orangutan.game;

import java.util.ArrayList;

import com.orangutan.game.App.SkeletonApp;

public class Tile {
    protected GameItem item;
    Tile[] neighbours = new Tile[8];

    //a prototipus kimenethez
    public String id;
    public String name;

    public Tile[] getNeighbours() {
        return neighbours;

    }

    public Tile() {
    }


    public Tile(String id) {
        this.id = id;
    }

    public GameItem getItem() {
        return item;
    }

    public void setItem(GameItem g) {
        item = g;
        g.currentTile = this;
    }

    public Tile getNeighbour(Direction d) {
        switch (d) {
            case A:
                return neighbours[0];
            case S:
                if (neighbours.length > 1) {
                    return neighbours[1];
                }

            case D:
                if (neighbours.length > 2) {
                    return neighbours[2];
                }
            case F:
                if (neighbours.length > 3) {
                    return neighbours[3];
                }
            case G:
                if (neighbours.length > 4) {
                    return neighbours[4];
                }
            case H:
                if (neighbours.length > 5) {
                    return neighbours[5];
                }
            case J:
                if (neighbours.length > 6) {
                    return neighbours[6];
                }
            case K:
                if (neighbours.length > 7) {
                    return neighbours[7];
                }

        }
        return null;
    }

    public void steppedOn(Animal g) {
        item = g;
        g.Enter();
        g.setCurrentTile(this);
    }

    public void remove() {
        item = null;
    }

    public void setNeighbours(Tile[] neighbours) {
        this.neighbours = neighbours;
    }

    public String getId() {
        return id;
    }

    ;


    public void stat() {
        System.out.println(id);
        System.out.println("\n");
        if (this.getItem() != null) {
            System.out.println("\t " + "GameItem: " + this.getItem().id);
        } else {
            System.out.println("\t " + "GameItem:  NULL");
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}