package com.orangutan.game;
import com.orangutan.game.App.SkeletonApp;

public class ExitTile extends Tile {

    private Tile entryTile;

    public ExitTile() {
    }
    public ExitTile(String id, Tile t) {
        this.id = id;
        this.entryTile=t;
    }

    public void setEntryTile(Tile t){
        entryTile = t;
    }
    public void steppedOn(Animal g){

        entryTile.steppedOn(g);
        g.exit();
    }
}
