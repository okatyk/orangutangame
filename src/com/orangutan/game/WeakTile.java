package com.orangutan.game;

import com.orangutan.game.App.SkeletonApp;
import com.orangutan.game.view.View;

public class WeakTile extends Tile {
    private int health;

    public WeakTile() {
    }

    public WeakTile(String id) {
        this.id = id;
    }
    @Override
    public void steppedOn(Animal g) {
        item = g;
        g.Enter();
        g.setCurrentTile(this);
        health--;
        if (health == 0) {
            breakTile();
        }
    }

    public void breakTile() {
        getItem().die();
        Game.removeTile(this);
        //delete this
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }
    
    @Override
    public void stat() {
        super.stat();
        System.out.println("\t " + "Health: " + getHealth());
        }
    }

