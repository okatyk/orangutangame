package com.orangutan.game;

import com.orangutan.game.App.SkeletonApp;

/**
 * @author Bence
 * Bizonyos idokozonkent sipol egyet, es megkerdezi a csempet,
 * amin all, hogy kik a szomszedos csempek, majd a szomszedos
 * elemeken levo pandakat ertesiti a sipolasrol.
 */
public class ChocoMachine extends Machine {


    public ChocoMachine(String id, Tile tile) {
        this.id = id;
        currentTile = tile;
    }

    @Override
    public void stat() {
        System.out.println("ChocoMachine");
        System.out.println("\n");
        System.out.println("\t " + "Tile: " + this.getCurrentTile().getId());
    }


    /**
     * Egyparamteres konstruktor
     *
     * @param timePeriod a sipolas idointervalluma
     */
    public ChocoMachine(int timePeriod) {
        super(timePeriod);
    }

    /* (non-Javadoc)
     * @see com.orangutan.game.GameItem#tack()
     * megkerdezi a csempet, amin all, hogy kik a szomszedos csempei.
     *  A szomszedos csempeket vegigkerdezi, hogy mik vannak rajtuk
     *   es ha nem ures, akkor ertesiti a csempen allo objektumot.
     *    Ha ures a szomszedos csempe, akkor nem hat ra.
     */
    @Override
    public void tack() {
        timeForSound--;
        if (timeForSound <= 0) {
            Tile[] negihbs = currentTile.getNeighbours();
            for (int i = 0; i < negihbs.length; i++) {
                if (negihbs[i] != null) {
                    if (negihbs[i].getItem() != null) {
                        GameItem it = negihbs[i].getItem();
                        it.reactionForWhistle();
                    }
                }
            }
            timeForSound = time;
        }
    }

    @Override
    public void die() {

    }
}
