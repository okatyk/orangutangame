package com.orangutan.game;

import com.orangutan.game.App.SkeletonApp;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/**
 * @author Barnabas Gnam
 * @since 2019-03-21
 */
public class Orangutan extends Animal {
    private boolean userControlled = false;
    private Panda inHandPanda = null;
    private int stepsToSteal = 0;
    private int points = 0;
    private Map map;

    public Orangutan(Map map) {
        this.map = map;
    }

    public Orangutan(boolean userControlled, Map map) {
        this.userControlled = userControlled;
        this.map = map;
    }

    public Orangutan(String id, Tile t) {
		this.id = id;
		this.currentTile = t;
	}

	@Override
    public void collideWith(GameItem g) {

    }
    
    public void releasePandas() {
    	if (inHandPanda != null) {
    	inHandPanda.releaseAll();
        	inHandPanda = null;
    	}
    	
    }

    public void setPanda(Panda p) {
        /*Ha nincs a kezeben panda */
        if (inHandPanda == null) {
            inHandPanda = p;
            p.setForwardAnimalInHand(this);
            /* Ha van mar pandaja */
        } else {
            Panda old = inHandPanda;
            inHandPanda = p;
            p.setForwardAnimalInHand(this);
            p.setBackPandaInHand(old);
        }
    }

    public boolean havePandaInHand() {
        return (inHandPanda != null);
    }

    public void exit() {
        Panda tmp = null;
        if (inHandPanda != null) {

            if (inHandPanda.getBackPandaInHand() != null) {
                tmp = inHandPanda.getBackPandaInHand();
            }
            
            inHandPanda.die();
            points++;
            inHandPanda = tmp;
        }
    }

    public void move() {
        if (getNextDirection() != null) {
            lastTile = currentTile; //proto

            Tile t = this.currentTile.getNeighbour(getNextDirection());
            if (t != null) {
                GameItem item = t.getItem();
                /* Ha ures a csempe */
                if (item == null) {
                    t.steppedOn(this);

                    if (inHandPanda != null) {
                     //   inHandPanda.setNextDirection(getNextDirection());
                        inHandPanda.setNextTile(lastTile);
                        inHandPanda.move();
                    }
                    /* Ha nem ures a csempe */
                } else {
                    /* Ha nem volt rablas 3 lepes ota */
                    if (stepsToSteal == 0) {
                        item.hitByOrangutan(this);

                    } else {
                        stepsToSteal--;
                    }

                }
            }
        } else {
            //System.out.println("nextdirection = null!");
        }

        //lepes utan kinullazzuk ,hogy kovetkezo tack()nal ne lepjen ha nincs uj irany.
        setNextDirection(null);
    }


    @Override
    public void stat() {
        System.out.println(id);
        System.out.println("\n");
        if (this.getInHandPanda() != null) {
            System.out.println("\t " + "in Hand Panda: " + this.getInHandPanda().id);
        } else {
            System.out.println("\t " + "in Hand Panda:  NULL");
        }
        System.out.println("\t " + "Steps to steal: " + getStepsToSteal());


    }

    /* (non-Javadoc)
     * @see com.orangutan.game.GameItem#hitByOrangutan(com.orangutan.game.Orangutan)
     *
     * Orangutan utkozik egy orangutannal
     * Ha van panda a kezeben es a hivonak nincs, akkor atadja a hivonak
     * a panda-lancot es magat nullazza.
     * Ekkor rablas tortent, tehat 3 lepes utan rabolhat ujra.
     */
    @Override
    public void hitByOrangutan(Orangutan o) {

        if (inHandPanda != null) {

            if (!o.havePandaInHand()) {
                o.setPanda(inHandPanda);
                inHandPanda = null;
                stepsToSteal = 3;

            }
        }
    }

    @Override
    public void hitByPanda(Panda p) {
        // ugy vesszuk mintha o menne neki
        p.hitByOrangutan(this);
    }

    @Override
    public void die() {
        if(userControlled){
            Game.endGame();
        }else {
            if(inHandPanda!=null){
                inHandPanda.releaseAll();
            }
            map.killOrangutan(this);
        }
    }

    public Panda getInHandPanda() {
        return inHandPanda;
    }

    public void setInHandPanda(Panda inHandPanda) {
        this.inHandPanda = inHandPanda;
    }

    public int getStepsToSteal() {
        return stepsToSteal;
    }

    public void setStepsToSteal(int stepsToSteal) {
        this.stepsToSteal = stepsToSteal;
    }

    public int getPoints() {
    	return points;
    }

}
