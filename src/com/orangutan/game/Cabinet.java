package com.orangutan.game;

public class Cabinet extends GameItem {

    private Cabinet siblingCabinet;
    private Tile toTeleportTile;  //A cabinet elotti tile, ahova kilep az animal a szekrenybol

    public Cabinet(String id,Tile tile) {
        super.setId(id);
        super.setCurrentTile(tile);
    }
    public Cabinet(String id,Tile tile, Tile t2) {
        super.setId(id);
        super.setCurrentTile(tile);
        setToTeleportTile(t2);
    }

    @Override
    public void stat() {
        System.out.println("Cabinet");
        System.out.println("\n");
        System.out.println("\t "+"Tile: "+this.getCurrentTile().getId());
        //Esetleg meg a siblingscabinetet idelehet rakni
    }






    @Override
    public void hitByOrangutan(Orangutan o) {
        Tile oda = getSiblingCabinet().getToTeleportTile();
        if (oda != null) {
        	if (oda.getItem() == null) {
                oda.steppedOn(o);
            }
        }

    }

    @Override
    public void hitByPanda(Panda p) {
        // TODO Auto-generated method stub
        Tile oda = getSiblingCabinet().getToTeleportTile();
        if (oda != null) {
            if (oda.getItem() == null) {
                oda.steppedOn(p);
            }
        }
    }

    @Override
    public void tack() {
        // TODO Auto-generated method stub

    }

    public Cabinet getSiblingCabinet() {
        return siblingCabinet;
    }

    public void setSiblingCabinet(Cabinet siblingCabinet) {
        this.siblingCabinet = siblingCabinet;
    }

    @Override
    public void die() {

    }

    public Tile getToTeleportTile() {
        return toTeleportTile;
    }

    public void setToTeleportTile(Tile toTeleportTile) {
        this.toTeleportTile = toTeleportTile;
    }
}
	