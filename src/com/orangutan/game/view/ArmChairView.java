package com.orangutan.game.view;

import java.awt.Graphics;
import java.awt.Image;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import com.orangutan.game.*;
/**
 * 
 * @author Bandi
 *
 */
public class ArmChairView implements Drawable{
	protected ArmChair armchair;
	protected View view;
	protected Image image;
	/**
	 * Konstruktor
	 * @param a hozza tartozo fotel
	 * @param v	a view
	 */
	public ArmChairView(ArmChair a, View v) {
		armchair = a;
		view = v;
		
		try {
			image = ImageIO.read(new File("armchair.png"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	/**
	 * Kirajzolja az adott fotelt
	 */
	@Override
	public void Draw(Graphics g) {
		TileView tv = view.getTileView(armchair.getCurrentTile()); // elkeri az eppen akutualis csempe TileView peldanyat
		if (tv != null) {
			g.drawImage(image, tv.getX()+10, tv.getY(), view); //view: observer - object to be notified as more of the image is converted.
		}
	}

	@Override
	public GameItem getGameItem() {
		return armchair;
	}
}
