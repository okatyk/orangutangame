package com.orangutan.game.view;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;

import com.orangutan.game.*;

/***
 * AnimalView Abstract ősosztály a pandaknak
 */
public abstract class AnimalView implements Drawable {
	protected Panda animal;										//atirtam Pandara, mert igy tudtam megvalositani a huzott pandak osszekoteset
	protected View view;
	protected Image image; // ezt a kepet tolti be majd a konkret allat
	
	/**
	 * Konstruktor
	 * @param a hozza tartozo panda
	 * @param v	a view
	 */
	public AnimalView(Panda a, View v) {
		animal = a;
		view = v;
	}
	/**
	 * kirajzolja a pand�t, valamit, ha koveti ot panda osszekoti oket
	 */
	@Override
	public void Draw(Graphics g) {
		TileView tv = view.getTileView(animal.getCurrentTile()); // elkeri az eppen akutualis csempe TileView peldanyat

		TileView tvBehind = null;
		if(animal.getBackPandaInHand() != null)
			tvBehind = view.getTileView(animal.getBackPandaInHand().getCurrentTile());  //mogotte levo panda csempeje

		if(tvBehind != null) {
			int align = tv.getSize(); //kozepen legyen a vonal
			g.setColor(Color.GREEN);
			g.drawLine(tv.getX()+align, tv.getY()+align, tvBehind.getX()+align, tvBehind.getY()+align); //vonal az orangutan es a huzott panda koze
		}

		if (tv != null) {
			g.drawImage(image, tv.getX()+10, tv.getY(), view); //view: observer - object to be notified as more of the image is converted.
		}
		
	}
	
	public Panda getPandaFromView() {
		return animal;
	}

	public GameItem getGameItem(){
		return animal;
	}
}
