package com.orangutan.game.view;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.*;

import com.orangutan.game.Panda;
import com.orangutan.game.Tile;

/**
 * @author Bence
 *
 */
public class View extends JPanel {

	private static final long serialVersionUID = 1L;
	private JFrame window; //az foablak amihez hozzadja magat
	private Image background; //hatterkep
	private int pointsToDraw = 0;
	private Font font;
	
	private List<Drawable> drawables; // a kirajzolando objektumok listaja
	private HashMap<Tile, TileView> tilePairs; // a modellbeli Tile-t es a view-beli TileView-t koti ossze
	
	public View() {
		super(true);
		drawables = new ArrayList<>();
		tilePairs = new HashMap<>();
		window = new JFrame("Orangutan Game by Spiked Metal Hollyrollers");
		window.setMinimumSize(new Dimension(800, 700));
		window.add(this);
		font = new Font("Arial", Font.BOLD, 18);
		
		try {
			background = ImageIO.read(new File("bckgrnd.jpg"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		window.pack();
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		window.setVisible(true);
	}
	
	/** kirajzolja a hatteret es minden listaban szereplo objektumot
	 */
	public void paintComponent(Graphics g) {
		 super.paintComponent(g);
		 g.drawImage(background, 0, 0, this);
		 g.setFont(font);
		 g.drawString("Pontszam: " + pointsToDraw, 650, 650);
		 
		 if ( !(drawables.isEmpty()) ) {
			 for (Drawable drawable : drawables) {
					drawable.Draw(g);
				}
		 }
	}

	public void endGameDraw(){
		JOptionPane.showMessageDialog(window, "Game Over! \n your points is: "+pointsToDraw);


	}
	
	public int getHeight(){
		return window.getHeight();
	}
	
	public int getWidth(){
		return window.getWidth();
	}
	
	/** Berakja a modellbeli csempet a view-beli parjaval egyutt egy taroloba
	 * @param t modellbeli csempe
	 * @param tv view-beli csempe
	 */
	public void PutTile(Tile t, TileView tv) {
		tilePairs.put(t, tv);
	}
	
	/** Megadja a modellbeli csempehez tartozo view-beli csempet
	 * @param t modellbeli csempe
	 * @return view-beli csempe
	 */
	public TileView getTileView(Tile t) {
		return tilePairs.get(t);
	}
	
	public void AddDrawable(Drawable d) {
		drawables.add(d);
	}
	
	public void RemoveDrawable(Drawable d) {
		drawables.remove(d);
	}

	public List<Drawable> getDrawables() {
		return drawables;
	}

	public void setDrawables(List<Drawable> drawables) {
		this.drawables = drawables;
	}
	
	public void setPoints(int p) {
		pointsToDraw = p;
	}


}
