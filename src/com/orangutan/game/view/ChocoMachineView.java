package com.orangutan.game.view;

import java.awt.Graphics;
import java.awt.Image;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import com.orangutan.game.*;
/**
 * 
 * @author Bandi
 *
 */
public class ChocoMachineView implements Drawable{
	protected ChocoMachine chocomachine;
	protected View view;
	protected Image image;
	/**
	 * 
	 * @param cm hozza tartozo csokiautomata
	 * @param v a view
	 */
	public ChocoMachineView(ChocoMachine cm, View v) {
		chocomachine = cm;
		view = v;
		
		try {
			image = ImageIO.read(new File("chocomachine.png"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	/**
	 * Kirajzolja a csokiautomatat
	 */
	@Override
	public void Draw(Graphics g) {
		TileView tv = view.getTileView(chocomachine.getCurrentTile()); // elkeri az eppen akutualis csempe TileView peldanyat
		if (tv != null) {
			g.drawImage(image, tv.getX()+10, tv.getY(), view); //view: observer - object to be notified as more of the image is converted.
		}
	}

	@Override
	public GameItem getGameItem() {
		return chocomachine;
	}
}
