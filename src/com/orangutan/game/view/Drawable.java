
package com.orangutan.game.view;

import com.orangutan.game.GameItem;

import java.awt.Graphics;

/**
 * @author Bence
 * A kirajzolando objektumok interfesze
 */
public interface Drawable {

	public void Draw(Graphics g);

	GameItem getGameItem();
}
