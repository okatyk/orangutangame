package com.orangutan.game.view;

import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import com.orangutan.game.*;
/**
 * 
 * @author Bandi
 *
 */
public class ShyPandaView extends AnimalView {
	/**
	 * Konstruktor, beallitja a kepet
	 * @param a hozza tartozo felos panda
	 * @param v a view
	 */
	public ShyPandaView(Panda a, View v) {
		super(a, v);
		
		try {
			image = ImageIO.read(new File("shypanda.png"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
}
