package com.orangutan.game.view;

import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import com.orangutan.game.*;
/**
 * 
 * @author Bandi
 *
 */
public class LazyPandaView extends AnimalView {
	/**
	 * Konstruktor, beallitja a kepet
	 * @param a hozza tartozo lusta panda
	 * @param v a view
	 */
	public LazyPandaView(Panda a, View v) {
		super(a, v);
		
		try {
			image = ImageIO.read(new File("lazypanda.png"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
