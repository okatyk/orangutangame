package com.orangutan.game.view;

import java.awt.Graphics;
import java.awt.Image;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import com.orangutan.game.Cabinet;
import com.orangutan.game.GameItem;

public class CabinetView implements Drawable {
	protected Cabinet cabinet;
	protected View view;
	protected Image image; // ezt a kepet tolti be majd a konkret allat
	
	public CabinetView(Cabinet a, View v) {
		cabinet = a;
		view = v;
		
		try {
			image = ImageIO.read(new File("cabinet.png")); 
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void Draw(Graphics g) {
		TileView tv = view.getTileView(cabinet.getCurrentTile()); // elkeri az eppen akutualis csempe TileView peldanyat
		if (tv != null) {
			g.drawImage(image, tv.getX()+15, tv.getY(), view); //view: observer - object to be notified as more of the image is converted.
		}
		
	}

	@Override
	public GameItem getGameItem() {
		return cabinet;
	}

}
