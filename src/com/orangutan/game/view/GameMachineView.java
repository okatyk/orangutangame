package com.orangutan.game.view;

import java.awt.Graphics;
import java.awt.Image;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import com.orangutan.game.*;
/**
 * 
 * @author Bandi
 *
 */
public class GameMachineView implements Drawable{
	protected GameMachine gamemachine;
	protected View view;
	protected Image image;
	/**
	 * Konstruktor
	 * @param gm a hozza tartozo jatekautomata
	 * @param v a view
	 */
	public GameMachineView(GameMachine gm, View v) {
		gamemachine = gm;
		view = v;
		
		try {
			image = ImageIO.read(new File("gamemachine.png"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	/**
	 * Kirajzolja a jatekautomatat
	 */
	@Override
	public void Draw(Graphics g) {
		TileView tv = view.getTileView(gamemachine.getCurrentTile()); // elkeri az eppen akutualis csempe TileView peldanyat
		if (tv != null) {
			g.drawImage(image, tv.getX()+10, tv.getY(), view); //view: observer - object to be notified as more of the image is converted.
		}
	}

	@Override
	public GameItem getGameItem() {
		return gamemachine;
	}
}
