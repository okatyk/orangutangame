package com.orangutan.game.view;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;

import com.orangutan.game.GameItem;
import com.orangutan.game.Tile;

public class TileView implements Drawable{
	private Tile tile;
	private int size, x, y;
	private Color color;
	private View view;
	private String name;
	
	/** Beallitja a kirajzolando csempe parametereit
	 * @param t a hozzatartozo modellbeli csempe
	 * @param v view: ebbe teszi beli a csempe-part
	 * @param c csempe szine
	 * @param x csempe x pozicioja
	 * @param y csempe y pozicioja
	 * @param size csempekor merete
	 */
	public TileView(Tile t,View v, Color c, int x, int y, int size, String name) {
		tile = t;
		view = v;
		color = c;
		this.x = x;
		this.y = y;
		this.size = size;
		this.name = name;
		view.PutTile(t, this);
		
	}
	
	/**
	 * @return a csempe x koordinataja
	 */
	public int getX() {
		return x;
	}
	
	/**
	 * @return a csempe y koordinataja
	 */
	public int getY() {
		return y;
	}
	/**
	 * @return a csempe m�rete
	 */
	public int getSize() {
		return size;
	}

	/** kirajzol egy csempet a beallitott parameterekkel
	 */
	@Override
	public void Draw(Graphics g) {
		if(tile!=null) {
			g.setColor(color);
			g.fillOval(x, y, size, size);

			g.setColor(Color.white);
			g.fillOval(x,y,size/3,size/3);

			g.setColor(Color.BLUE);

			g.drawString(tile.name,x+4,y+17);
			Tile[] neighbours = tile.getNeighbours();
			for(int i=0; i<neighbours.length; i++) {
				if(neighbours[i]!=null) {
					g.setColor(Color.GRAY);
					g.drawLine(x+size/2, y+size/2, view.getTileView(neighbours[i]).getX()+size/2, view.getTileView(neighbours[i]).getY()+size/2);
				}

			}

		}
	}

	@Override
	public GameItem getGameItem() {
		return null;
	}


}
