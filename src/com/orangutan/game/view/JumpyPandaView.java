package com.orangutan.game.view;

import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import com.orangutan.game.*;
/**
 * 
 * @author Bandi
 *
 */
public class JumpyPandaView extends AnimalView {
	/**
	 * Konstruktor, beallitja a kepet
	 * @param a hozza tartozo ugros panda
	 * @param v a view
	 */
	public JumpyPandaView(Panda a, View v) {
		super(a, v);
		
		try {
			image = ImageIO.read(new File("jumpypanda.png"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}


}
