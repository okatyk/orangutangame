package com.orangutan.game.view;

import java.awt.*;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import com.orangutan.game.*;

/**
 * @author Bandi
 */
public class OrangutanView implements Drawable {
    protected Orangutan orangutan;
    protected View view;
    protected Image image;
    protected boolean isControlled;

    /**
     * Konstruktor
     *
     * @param a hozza tartozo orangutan
     * @param v a view
     */
    public OrangutanView(Orangutan a, View v,boolean as) {
        orangutan = a;
        view = v;
        isControlled=as;
        try {
            image = ImageIO.read(new File("orangutan.png")); //betoltesnel a projekt helyen kell lennie a kepnek
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    /**
     * Kirajzolja az orangutan, ha van mogotte allo panda osszekoti oket, osszekoti azzal a csempevel az orangutan amerre lepni fog
     */
    @Override
    public void Draw(Graphics g) {
        TileView tv = view.getTileView(orangutan.getCurrentTile()); // elkeri az eppen akutualis csempe TileView peldanyat
        TileView tvBehind = null;
        if (orangutan.havePandaInHand()) {
            tvBehind = view.getTileView(orangutan.getInHandPanda().getCurrentTile());
        }  //mogotte levo panda csempeje
        TileView tvFront = null;
        if (orangutan.getNextDirection() != null && orangutan.getCurrentTile().getNeighbour(orangutan.getNextDirection()) != null) {
            tvFront = view.getTileView(orangutan.getCurrentTile().getNeighbour(orangutan.getNextDirection()));
        }

        if (tvBehind != null) {
            int align = tv.getSize() / 2;
            g.setColor(Color.GREEN);
            g.drawLine(tv.getX() + align, tv.getY() + align, tvBehind.getX() + align, tvBehind.getY() + align); //vonal az orangutan es a huzott panda koze

        }
        if (tvFront != null) {
            int align = tv.getSize() / 2;

            g.setColor(Color.RED);
            g.drawLine(tv.getX() + align, tv.getY() + align, tvFront.getX() + align, tvFront.getY() + align);
        }
        if (tv != null) {
            g.drawImage(image, tv.getX() + 10, tv.getY() - 20, view); //view: observer - object to be notified as more of the image is converted.
        }
        if(isControlled){
            Tile tmp=orangutan.getCurrentTile();

           g.setColor(Color.black);
           g.setFont(new Font("default", Font.BOLD, 14));
            int asd=0;
            for(Direction d : Direction.values()){
                if(tmp.getNeighbour(d) != null){
                    g.drawString(d+"  "+tmp.getNeighbour(d).name,tv.getX()+tv.getSize()-20,tv.getY()+10*asd+tv.getSize()/2);
                    asd++;
                }
            }
        }

    }

    @Override
    public GameItem getGameItem() {
        return orangutan;
    }
}
