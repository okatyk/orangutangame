package com.orangutan.game;

import com.orangutan.game.GameItem;

public abstract class Machine extends GameItem {
	
	protected int timeForSound = 0;
	protected int time;
	
	/**
	 * @param timePeriod hangkiadasok kozti idointervallum
	 */
	public Machine(int timePeriod) {
		timeForSound = timePeriod;
		time = timePeriod;
	}
	
	public Machine() {
		timeForSound = 20;
		time = 20;
	}
	
	/**
	 * Prototipushoz: elsuti a hangkiadast explicit
	 */
	public void signal() {
		timeForSound = 0;
		tack();
	}

}